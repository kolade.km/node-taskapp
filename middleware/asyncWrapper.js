// eslint-disable-next-line consistent-return
const asyncWrapper = (callBack) => async (req, res, next) => {
  try {
    await callBack(req, res, next);
  } catch (error) {
    return error;
  }
};
module.exports = asyncWrapper;
