const request = require('supertest');
const { getTaskById } = require('./controller/taskapp.controller');
const getTaskByIdService = require('./controller/taskapp.controller');
const index = require('./index');

// let resJson = null
// let resSend = null
const payLoadEmptyId = {
  task: 'fix cars',
  id: '',

};
const payLoad = {
  task: 'fix cars',
  id: '1',

};

describe('Tasks API', () => {
  it('GETs all task', async () => {
    const response = await request(index)
      .get('/api/v1/tasks');
    expect(response.body)
      .toEqual(expect.arrayContaining([
        expect.objectContaining({
          id: expect.any(String),
          task: expect.any(String),
        }),
      ]));
  });

  it('POSTs task with empty ID', async () => {
    const response = await request(index)
      .post('/api/v1/tasks')
      .send(payLoadEmptyId);
    expect(response.body).toEqual('Please provide a new task with ID');
  });

  it('POSTs task ', async () => {
    const response = await request(index)
      .post('/api/v1/tasks')
      .send(payLoad);
    expect(response.body.message).toEqual('Task created');
  });
});
