const { db } = require('../dataBase');

exports.createTaskService = async (newTask) => {
  if (newTask.id !== '') {
    const createdTask = await db.collection('Task').doc(newTask.id.toString()).set(newTask);
    if (Object.keys(createdTask).length) {
      return { message: 'Task created' };
    } return { error: 'Try again later' };
  }
  return { error: 'Please provide a new task with ID' };
};

exports.getTaskByIdService = async (id) => {
  const task = await db.collection('Task').doc(id.toString()).get();
  if (task.exists) {
    return task.data();
  } return { error: 'No such task exist' };
};

exports.getTasksService = async () => {
  const tasks = await db.collection('Task').get();

  const taskList = [];
  tasks.forEach(
    (doc) => taskList.push(doc.data()),
  );
  return taskList;
};
exports.updateTaskService = async (taskToUpdate) => {
  console.log(taskToUpdate);
  const updatedTask = await db.collection('Task').doc(taskToUpdate.id.toString()).update(taskToUpdate);
  return updatedTask;
};
