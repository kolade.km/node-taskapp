const express = require('express');

const app = express();
const taskApp = require('./routes/taskApp.route');

app.use(express.json());

app.use('/', taskApp);

module.exports = app;
