const {
  createTaskService, getTaskByIdService, getTasksService, updateTaskService,
} = require('../service/fireStore');
const asyncWrapper = require('../middleware/asyncWrapper');

const getAllTask = asyncWrapper(async (req, res) => {
  const tasks = await getTasksService();
  if (Object.keys(tasks).length === {}) {
    res.status(400).json(tasks.error);
  }
  res.json(tasks);
});

const getTaskById = asyncWrapper(async (req, res) => {
  const { id } = req.params;
  const task = await getTaskByIdService(id);
  if (task.error === 'No such task exist') {
    res.status(400).json(task.error);
  }
  res.json(task);
});

const createTask = asyncWrapper(async (req, res) => {
  const newTask = req.body;
  const createdTask = await createTaskService(newTask);
  if (createdTask.error) { res.status(400).json(createdTask.error); }
  res.send(createdTask);
});

const updateTask = asyncWrapper(async (req, res) => {
  const taskToUpdate = req.body;
  taskToUpdate.id = req.params.id;

  const isATask = await getTaskByIdService(taskToUpdate.id);

  if (isATask.id === taskToUpdate.id) {
    const updatedTask = await updateTaskService(taskToUpdate);
    res.send(updatedTask);
  }
  res.status(400).json(isATask);
});

const deleteTask = asyncWrapper(async (req, res) => {
  res.send('you are here');
  // res.send(error);
});

module.exports = {
  deleteTask, createTask, getTaskById, updateTask, getAllTask,
};
