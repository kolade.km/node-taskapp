const admin = require('firebase-admin');

const serviceAccount = require('./node-taskapp-firebase-adminsdk-1j8ls-c8f5c61fc8.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});

exports.db = admin.firestore();
