const dotenv = require('dotenv');

dotenv.config();

const {
  API_KEY, AUTH_DOMAIN, PROJECT_ID, MESSAGING_CENTER_ID, APP_ID, MEASUREMENT_ID,
  STORAGE_BUCKET, MESSAGINGSENDER, HOST_URL,
} = process.env;

module.exports = {
  host: HOST_URL,
  firebaseConfig: {
    apiKey: API_KEY,
    authDomain: AUTH_DOMAIN,
    projectId: PROJECT_ID,
    messagingCenterId: MESSAGING_CENTER_ID,
    appId: APP_ID,
    storageBucket: STORAGE_BUCKET,
    messagingSender: MESSAGINGSENDER,
    measurmentId: MEASUREMENT_ID,
  },
};
