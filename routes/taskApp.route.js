const express = require('express');

const router = express.Router();
const {
  getTaskById, createTask, updateTask, deleteTask, getAllTask,
} = require('../controller/taskapp.controller');

router.get('/api/v1/tasks', getAllTask);
router.get('/api/v1/task/:id', getTaskById);
router.post('/api/v1/tasks', createTask);
router.put('/api/v1/task/:id', updateTask);
router.delete('/api/v1/task/:id', deleteTask);

module.exports = router;
